
var notes = [];
loadList();
function saveList() {
	/*global localStorage*/
	localStorage.notes = JSON.stringify(notes);
}

function loadList() {
	if (localStorage.notes) {
		notes = JSON.parse(localStorage.notes);
		displayList();
	}
}

function addItem() {
	var textbox = document.getElementById('item');
	var itemText = textbox.value;
	textbox.value = '';
	textbox.focus();
	var newItem = {title: itemText, quantity: 1};
	var isInNotes = false;
	notes = notes.map(function(note){
		if(note.title === newItem.title){
			isInNotes = true;
			note['quantity']+=1;
			return note;
		}
		return note;
	});
	if(!isInNotes){
		notes.push(newItem);
	}
	saveList();
	displayList();
}

function displayList() {
	var table = document.getElementById('list');
	table.innerHTML = '';
	for (var i = 0; i<notes.length; i++) {
		var node = undefined;
		var note = notes[i];
		var node = document.createElement('tr');
		var html = '<td>'+note.title+'</td><td>'+note.quantity+'</td><td><a href="#" onClick="deleteIndex('+i+')">delete</td>';
		node.innerHTML = html;
		table.appendChild(node);
	}
}

function deleteIndex(i) {
	notes.splice(i, 1);
	saveList(notes);
	displayList();
}

var button = document.getElementById('add');
button.onclick = addItem;
