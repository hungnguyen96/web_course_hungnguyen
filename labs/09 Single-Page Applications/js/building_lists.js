
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

var table = document.createElement('table');
var heading = document.createElement('tr');
heading.innerHTML="<th>Title</th><th>Year</th>";
table.appendChild(heading);
for (var i=0; i < books.length; i++) {
	var row = document.createElement('tr');
	row.innerHTML="<td>"+books[i].title+"</td><td>"+books[i].year+"</td>" ;
	table.appendChild(row);
} 
var list =table.getElementsByTagName("tr");
console.log(list);
for(var i=0; i< list.length;i++){
	list[i].addEventListener("click",function(element){
		const header = document.createElement('h1');
		header.innerHTML=element.target.innerHTML;
		document.body.removeChild(document.body.firstChild);
		document.body.insertBefore(header, document.body.firstChild);
	});
}
document.body.appendChild(table);
